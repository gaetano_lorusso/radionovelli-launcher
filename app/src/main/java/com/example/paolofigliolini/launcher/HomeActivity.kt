package com.example.paolofigliolini.launcher

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.media.Image
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import java.util.ArrayList
import android.support.v4.content.ContextCompat.startActivity



class HomeActivity : Activity() {
    private var manager: PackageManager? = null
    private var apps: MutableList<App>? = null
    private var imageViews: ArrayList<ImageView>? = null
    private var textViews: ArrayList<TextView>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        imageViews = ArrayList()
        textViews= ArrayList()
        imageViews!!.add(findViewById(R.id.image1) )
        imageViews!!.add(findViewById(R.id.image2))
        imageViews!!.add(findViewById(R.id.image3) )
        imageViews!!.add(findViewById(R.id.image4) )
        imageViews!!.add(findViewById(R.id.image5) )
        imageViews!!.add(findViewById(R.id.image6) )
        textViews!!.add(findViewById(R.id.textview1) )
        textViews!!.add(findViewById(R.id.textview2) )
        textViews!!.add(findViewById(R.id.textview3) )
        textViews!!.add(findViewById(R.id.textview4) )
        textViews!!.add(findViewById(R.id.textview5) )
        textViews!!.add(findViewById(R.id.textview6) )




        loadApps()
        loadListView()
    }


    private fun loadApps() {
        manager = packageManager
        apps = ArrayList<App>()

        val i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)

        val availableActivities = manager!!.queryIntentActivities(i, 0)
        for (ri in availableActivities) {
            val app = App()
            app.label = ri.loadLabel(manager)
            app.name = ri.activityInfo.packageName
            app.icon = ri.activityInfo.loadIcon(manager)
            apps!!.add(app)
        }
    }


    private fun loadListView() {
        for (i in imageViews!!.indices) {
            imageViews!![i].setImageDrawable(apps!![i].icon)
            imageViews!![i].setOnClickListener(View.OnClickListener {

                val i = manager?.getLaunchIntentForPackage(apps!!.get(i).name.toString())
                this.startActivity(i)
            })
            textViews!![i].setText(apps!![i].label)
        }
    }


}
